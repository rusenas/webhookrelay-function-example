# Automated Webhook Relay Function updates

This is an example of using Webhook Relay Bitbucket Pipelines pipe to automatically upload functions.

## Setup

1. Create a new function here https://my.webhookrelay.com/functions (or using `relay` CLI)
2. Get your access token key & secret https://my.webhookrelay.com/tokens
3. Configure Bitbucket repository settings with access token:
   - Go to "Repository settings"
   - Click on "Repistory variables" (if pipelines are not enabled, enable them)
   - Add two environment variables RELAY_KEY (with value from the access token 'key') and RELAY_SECRET (with value from the access token 'secret')

That's it, you can now push your functions and get them updated.